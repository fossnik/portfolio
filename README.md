![Build Status](https://gitlab.com/pages/hugo/badges/master/build.svg)

#[https://fossnik.gitlab.io/portfolio/](https://fossnik.gitlab.io/portfolio/)

## Hugo is a Static Site Generator

This [Hugo] website is derived from [Markdown], and hosted on [GitLab Pages](https://pages.gitlab.io).

A [CI/CD pipeline](#gitlab-ci) seamlessly spins up the HTML and CSS after each pushed commit.

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [GitLab CI](#gitlab-ci)
- [Building locally](#building-locally)
- [Hugo development server](#hugo-development-server)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml).

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install][] Hugo
1. Preview your project: `hugo server`
1. Add content
1. Generate the website: `hugo` (optional)

Read more at Hugo's [documentation][].

## Hugo development server

If you clone or download this project to your local computer and run `hugo server`,
your site can be accessed under `localhost:1313/hugo/`.

To start the Hugo server with drafts enabled: `hugo server -D`

The theme used is adapted from http://themes.gohugo.io/beautifulhugo/.

[ci]: https://about.gitlab.com/gitlab-ci/
[hugo]: https://gohugo.io
[install]: https://gohugo.io/overview/installing/
[documentation]: https://gohugo.io/overview/introduction/
[markdown]: https://en.wikipedia.org/wiki/Markdown
