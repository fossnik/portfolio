---
title: About me
subtitle: on a personal note...
comments: false
---
{{< figure src="/BraytonPoint.gif" >}}

---
### Proud craftsman of quality code

- I was born in Rhode Island, and have lived most of my life there, enjoying fishing in Narragansett Bay, cycling, playing tennis, and hiking.
- Free and Open Source Software is a long-time passion of mine; I first installed Debian Linux in 2003, on a 533 MHz COMPAQ PC (Pentium III with 2.1GB Quantum Bigfoot HDD). Today, Arch Linux is my platform of choice (Linux Mint a close second).
- During a 5th grade algebra class, I authored my first computer programs on a [Texas Instruments TI-83 calculator](https://www.ticalc.org/archives/files/authors/78/7898.html) in TI-BASIC

---

{{< figure src="/selfie-mountains.jpg" >}}


I started off in IT technical support, but my journey into software development has been a very enjoyable experience.
Learning about concepts that underpin development of quality software has been a passion for me.
I study things like user experience, and paradigms like OOP, FP, and TDD to inform my process, because it takes using the right tool for the right reason to drive the coherence that makes a team effort truly more than the sum of its parts.

---

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Using command history and expansions in the Bourne again shell<a href="https://twitter.com/hashtag/Bash?src=hash&amp;ref_src=twsrc%5Etfw">#Bash</a> <a href="https://twitter.com/hashtag/Unix?src=hash&amp;ref_src=twsrc%5Etfw">#Unix</a> <a href="https://twitter.com/hashtag/Linux?src=hash&amp;ref_src=twsrc%5Etfw">#Linux</a> <a href="https://twitter.com/nixcraft?ref_src=twsrc%5Etfw">@nixcraft</a> <a href="https://twitter.com/linuxtoday?ref_src=twsrc%5Etfw">@linuxtoday</a> <a href="https://t.co/Auo1TRXE9S">pic.twitter.com/Auo1TRXE9S</a></p>&mdash; Zack Hartmann (@zack_hartmann) <a href="https://twitter.com/zack_hartmann/status/1167482338789470209?ref_src=twsrc%5Etfw">August 30, 2019</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

----

{{< figure src="/Screenshot_2019-07-24_17-37-08-bugs-are-attracted-to-the-light.png" >}}
