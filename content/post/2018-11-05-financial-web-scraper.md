---
title: Web Scraper, API, DB, and UI for - Yahoo Finance!
subtitle: Demo Website
date: 2018-11-05
tags: ["Express.js", "React.js", "Redux.js", "jSoup", "SQLite"]
---

### [YzFinance.org](http://www.yzfinance.org)
My first full-stack website is a React.js SPA built to present crypto-currency market data which is scraped nightly from Yahoo! Finance.
The data is retrieved and interpreted by a Java web scraper, persisted to an SQLite database, and made accessible through a REST API for display by the React frontend.

#### - Frontend (Node.js)
- Express.js
- React.js
- Redux.js
- React Router

#### - Backend (Java)
- Jsoup (Web Scraper)
- systemd event timer
- Express REST API

#### - Database
- SQLite

#### - Web Hosting
- Linode
- Arch Linux


[Java Source Code (Jsoup Web Scraper)]([https://gitlab.com/fossnik/JsoupWebScraper-yahFin])
