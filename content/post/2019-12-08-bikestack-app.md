---
title: "BikeStack Mobile"
date: 2019-12-08
subtitle: "A React Native Application"
tags: ["React Native", "AWS", "Expo", "Hooks API", "TypeScript", "Cognito", "DynamoDB", "Amplify"]
---

A performance tracking and data visualization app for bicyclists.

{{< youtube uPDgIf4GmOA >}}

# Scan to open
With an Android phone, you can scan this QR code with your [Expo mobile App](https://play.google.com/store/apps/details?id=host.exp.exponent) to load this project immediately.
{{< figure src="/2019.12.08-bikestack-expo-qr-code.png" >}}
---
Visit this app at it's home on expo.io ([https://expo.io/@fossnik/bikestack](https://expo.io/@fossnik/bikestack))

---
# Features
- [TypeScript]( {{< relref "/post/2019-08-14-react-typescript.md" >}} )
- Amazon Web Services (AWS)
- AWS Amplify
- AWS Cognito user pools (secure user creation and authentication)
- AWS DynamoDB (highly scalable record persistence)
- React native navigation
- Expo

<!--more-->
