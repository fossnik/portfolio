---
title: Recipe Trader
subtitle: Culinary Social Network
date: 2019-07-09
tags: ["React.js", "MongoDB", "Express.js", "Heroku", "React Router", "JWT Token", "Redux.js"]
---

## https://recipetrader-demo.herokuapp.com/

I am very excited to announce [RecipeTrader](https://recipetrader-demo.herokuapp.com/) - my newest React.js demo website.
RecipeTrader is a social network for sharing culinary recipes.
It features user registration, login, and profile creation - all securely authenticated by [JSON Web Token](https://jwt.io/).
Users can post recipes, and even like and comment on other users' submissions.

This full-stack project employs a [MongoDB](https://www.mongodb.com/) backend, [Express.js](https://expressjs.com/) API, and a [React.js](https://reactjs.org/) frontend with [Redux.js](https://redux.js.org/) state management, and routes through [React-router](https://github.com/ReactTraining/react-router).
It is deployed via a [Heroku](https://heroku.com/) CI/CD pipeline, streamlined with Webpack, and accessible at [recipetrader-demo.herokuapp.com](https://recipetrader-demo.herokuapp.com/)

<!--more-->

#### - Frontend (Node.js)
- React.js
- Redux.js
- React Router

#### - API / Models
- Express.js
- MongoDB (Mongoose)

#### - Web Hosting
- Heroku

#### - Security Features
- JSON Web Token

#### - Bootstrap, CSS
- Bootstap components were utilized to avoid tedious CSS customization.

#### - CSS, Inline Styles, and Styled Components
Three different ways of managing styles are demonstrated.
- [Inline styles](https://www.lifewire.com/what-is-css-inline-style-3466446)
- [Styled components](https://www.styled-components.com/)
- [Bootstrap classes](https://getbootstrap.com/)

##### Other Notes
I used [PivotalTracker](https://www.pivotaltracker.com/) to create and manage tickets for this project.
This approach really boosts organization and priority management.
