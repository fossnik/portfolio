---
title: React Native
subtitle: A return to gigabyte-guzzling development 
tags: ["React Native", "Android Studio"]
date: 2019-07-11
---

Now that [RecipeTrader](https://recipetrader-demo.herokuapp.com/) is mostly fleshed out and working, I set my sights once again on React Native.
I had to re-download a lot of system images, Android Studio, Expo emulators and such, which I had freed up for space.
The downloading alone makes this dev-setup a very protracted process.
{{< figure src="/Screenshot_2019-07-10_16-16-35-reactnative.png" >}} 
 
It's also extremely rough on space.
Here I am, once again precariously close to maxing out the meager 48GiB EXT4 volume which has been my home for years now.
{{< figure src="/Screenshot_2019-07-11_10-06-25-gparted.png" >}}

Additionally, while emulating phone hardware with [Android Studio](https://developer.android.com/studio/index.html), I only can expect about 90 minutes of battery life, significantly less than my typical 180 minutes.
<!--more-->

#### Why should I use such a foolishly small partition?
Expedience - I use [dd](https://en.wikipedia.org/wiki/dd_(Unix)) for backups.

#### Android Studio - Google and JetBrains
[Android Studio](https://en.wikipedia.org/wiki/Android_Studio) is by Google, but actually based on the [IntelliJ](https://en.wikipedia.org/wiki/IntelliJ_IDEA) platform.
Fortunately, I already have a license to [JetBrains](http://jetbrains.com/) products, which makes it easy to install and manage with JetBrains Toolbox.
If you are a student with a *.edu* email address, you can also get a [free student license](https://www.jetbrains.com/student/) for hundreds of dollars worth of JetBrains products, such as IntelliJ and WebStorm 

{{< figure src="/Screenshot_2019-07-11_10-41-51-jetbrains-toolbox.png" >}} 
