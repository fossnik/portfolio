---
title: "Linting and Prettier"
date: 2020-01-27
subtitle: "Using Linting and Code Cleanup"
tags: ["Node.js", "JavaScript", "Linting", "Prettier"]
draft: true
---

How to set up Webstorm with ESLint and Prettify to ensure formatting sameness between members of a team that also uses VSC? Follow

I'm part of a team where we need to ensure that the code between all of us is formatted the same.  

I've got a basic understanding of ESLint and Prettify and have setup WebStorm to use both.   

One issue I'm having is that when I 'correct ESLint errors' through WebStorm, the formatting ends up different (in terms of whitespace) than when I run 'prettify'.  So the formatting depends on which I run first. 

The second is that I'd like to have this all done automatically, on save.  I have searched and found various instructions on how to do this, but in light of the above, I'm not sure how best to run ESLint and Prettify to make sure that the code is consistently formatted.

Finally, 2/3 of the team is using VSC, so I'm wondering how we can all work together to keep the code the same.  That is, do we just set up our '.rc' files to be the same, and then each of us does what his/her IDE requires to enforce format on save?  

If there's a link that explains all the issues above, that would be great.   Thanks for any help.

My .eslintrc is 

{
  "extends": ["airbnb-base", "prettier"],
  "plugins": ["prettier"],
  "env": {
    "node": true,
    "mocha": true
  },
  "rules": {
    "comma-dangle": ["error", "never"],
    "global-require": 0
  }
}

and my .prettierrc is

{
    "useTabs": true,
    "printWidth": 140,
    "tabWidth": 2,
    "singleQuote": true,
    "trailingComma": "none"
}
