---
title: Using React Hooks API
subtitle: An alternative to Redux
tags: ["React", "React Hooks API"]
date: 2019-08-22
---


{{< figure src="/2019.08.31-React-Hooks-API.png" >}} 

The *hooks api* is a game-changing new framework introduced by the Facebook team in *React 16.8*.
It represents a radical foray into a totally new paradigm of React.js development.

# Notes on Hooks
Hooks are like primitive composable components for React.
With hooks, instead of using *ES2015 classes* and complex third party frameworks such as *Redux*, we can use simple ```function()``` expressions throughout.

## Functional Programs
They are more functional - essentially they trade the complexity of how classes work for the complexity of how closures work.
Hooks are more consistent with functional programming.

## Side-Effects
I had previously associated side-effects with a negative connotation, but in this context, side-effects are very much intentional.
A side-effect, or simply *effect*  is employed anytime we perform a fetch operation or write something out to the console in a class component.

## UseState
One of the most exciting parts of the Hooks API is the global state functionality.  With ```useState```, we have native functionality in React that might finally make it possible to avoid *Redux* entirely.
At long last, React developers can make use of *global state* without cluttering up our applications with loads of messy ```connect``` functions and the 10 pounds of  *actions*, *reducers*, and *lifecycle triggers* code it takes to get an ounce of functionality in React.
<!--more-->





## UseEffect
Use effect takes two arguments

## Closures
Not only did hooks greatly simply my program, they allowed me to work more effectively with closures, and create more robust functional programming

## Cleanups
Cleanup functions are powerful, because we can use functional expressions instead of *ES2015 classes*

