---
title: "Creating Animated GIFs using a Bash pipe"
date: 2020-01-08
subtitle: "How I create superior-quality GIFs using FFMPEG and ImageMagic"
tags: ["animated GIF", "imagemagick", "FFMPEG", "Linux"]
---

A coding friend [asked me recently](https://twitter.com/zack_hartmann/status/1182305266441826309) how I created my superior quality animated GIF demos of running Cypress e2e testing my applications.

I have to confess - this one-liner truly gives me utmost pleasure in life.

{{< figure src="/2019.09.29-yzfinance-cypress-tests.gif" >}} 

# Tools
- FFMPEG audio/video swiss-army knife [FFMPEG](https://ffmpeg.org/)
- ImageMagic ```convert``` - better aGIF generator than FFMPEG [ImageMagick](https://imagemagick.org/index.php)
- Bash command line pipe [Bash](https://www.gnu.org/software/bash/)
 
 <!--more-->

# The One-liner

    ffmpeg -i screen_capture-01.mp4 -f image2pipe -vcodec ppm - | convert - -layers Optimize final.gif

Here is how I automatically convert to animated GIF the last video in my folder.

    ffmpeg -i $(screen_capture*.mp4 | fold | tail -n 1) -f image2pipe -vcodec ppm - | convert - -layers Optimize final.gif
