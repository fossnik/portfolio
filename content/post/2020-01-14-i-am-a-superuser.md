---
title: "I am a Super User"
date: 2020-01-14
subtitle: "Is Learning more useful than Knowledge?"
tags: ["Stack Overflow", "SuperUser"]
---

Perhaps the greatest misconception I held as a novice computer programmer was that software was about "knowledge".
An important realization I had is that the much more significant skill is actually "learning".  After all, how do those who have knowledge acquire it?  Through learning, of course!
This is especially true of web development technologies, which seem to evolve much quicker than languages like Java, or C++, for example.

I am a superuser.  I have been using the web to find answers and learn new skills my entire life.

<!--more-->

I have talked about [Stack Overflow]( {{< relref "/post/2018-12-12-solving-on-stackoverflow.md" >}} ) in the past.
These types of forums are invaluable for seeking out the kinds of answers that it takes to do software development.

{{< figure src="/2020.01.14-SuperUser.jpg" >}}
