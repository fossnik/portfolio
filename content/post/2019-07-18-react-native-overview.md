---
title: React Native - Learn once, Write everywhere
subtitle: "or: How I learned to Stop Worrying and Love the 'book (Facebook)"
tags: ["React Native", "Android Studio", "iOS", "Babel"]
date: 2019-07-18
---

[React Native](https://facebook.github.io/react-native/) is a technology that makes it possible to create apps that work on multiple platforms, such as Android and iOS.

{{< figure src="/react-native-android-ios.png" >}} 

### Strengths
- Created by Facebook.
- Write one app that works on both iOS and Android.
- Groundswell of support. Find solutions to common problems easily.
- Open source - broad ecosystem & robust community
- React Native is used internally by Facebook, so we can rest assured that it will continue to be well-supported in the future.

### Weaknesses
- Minor differences between the Android and iOS platforms may require redress.
- Inherently asynchronous.
- Many native device features require 3rd party packages.
- Fast moving target - still in active development, monthly new versions frequently include breaking changes.
- React Native acts as a bridge between JS and native platform code, so it's not as efficient as native apps written with Java (Android), or Swift / Objective-C (iOS).


### What is a React Native app?

Although based on the React framework of web-development fame, React Native apps are NOT web-apps or websites.
 This is because the UI components are [expressed by native platform code](https://hackernoon.com/react-native-bridge-for-android-and-ios-ui-component-782cb4c0217d).
  React Native allows us to build something similar to a React.js web app, but essentially replaces the React's HTML DOM with native UI components.
  

    npx --package react-native-cli react-native

<!--more-->

### Does React Native work the same way as React?

Not exactly - React Native avails a DOM which is implemented in the native UI code of the respective Android and iOS platforms.
 This "bridge" between the JS and native code is associated with a performance hit, however this is often well within allowable tolerances.

However, all the things you know about React, such as lifecycle hooks, will apply to working with React Native. 
 This is because even though the UI is compiled into native platform code, the JavaScript logic is not.
  React Native hosts the JS thread in an environment, just like how Node.js allows JS code to run outside the browser.


#### Native Code counterparts for DOM elements as seen in React Native

{{< figure src="/Screenshot_2019-07-19_10-24-35-RN-components.png" >}}

React Native is a "Bridge" between JS and Native Platform Components.
As you can see, React Native uses different components than traditional React.js 

##### Defining state in React.js

    constructor(props) {
      super(props);

      this.state = {
        myAppName: "example"
      };
    }

##### Defining state in React Native

    state = {
      myAppName: "example"
    }

## React Native References
- http://facebook.github.io/react-native/docs/getting-started
- https://github.com/vhpoet/react-native-styling-cheat-sheet

## Notes
React Native *emulates* CSS styling, but does not work in an identical way.

#### Flexbox
React Native uses [Flexbox](https://medium.com/the-react-native-log/a-mini-course-on-react-native-flexbox-2832a1ccc6) to position elements


- The ```flex``` property defines how much the entire element should grow (flex-grow)
- In the case of ```flex: 1```, the element should take up as much space as is available (rather than as much space as is needed for the contents)
- The Main Axis is defined by ```flexDirection: <column|row>```
- '```justifyContent: '<flex-start|space-between>```' defines how elements are positioned on the Cross Axis
- '```alignItems: '<flex-start|center>```' defines where elements are positioned on the Cross Axis


#### Creating Styles
React Native uses the ```StyleSheet``` element to create styles
[StyleSheet](https://facebook.github.io/react-native/docs/stylesheet.html)

    import {StyleSheet} from 'react-native'
    
    const styles = StyleSheet.create({
      footer: {
        flex: 1
      }
    });
    
    
#### The View Component
Styles are typically applied to a [view component](https://facebook.github.io/react-native/docs/view.html).
Many other elements, such as the ```Text``` element, are not as style-able.

#### The Touchable Component
Like in web React.js, every React Native component has it's own set of properties and events.
Wrap any component in a touchable component to make it (you guessed it) - touchable
- [TouchableHighlight](https://facebook.github.io/react-native/docs/touchablehighlight)
- [TouchableNativeFeedback](https://facebook.github.io/react-native/docs/touchablenativefeedback)
- [TouchableOpacity](https://facebook.github.io/react-native/docs/touchableopacity)
- [TouchableWithoutFeedback](https://facebook.github.io/react-native/docs/touchablewithoutfeedback)


      <TouchableHighlight onPress={props.onPressed}>
          <View>
              <Text>{props.name}</Text>
          </View>
      </TouchableHighlight>

#### The ScrollView Component
[ScrollView](https://facebook.github.io/react-native/docs/scrollview.html) enables scrolling down to view more content.
Without ScrollView, all content that was beyond the length of the initial viewport would simply be cut off.

#### The FlatList Component
[FlatList](https://facebook.github.io/react-native/docs/flatlist.html) is an alternative to ScrollView, which only renders what is needed on the screen, which is helpful for slower devices.
FlatList accepts an array as input, so that you don't have to use mapping.
It is the best solution for rendering growing, dynamic lists.
There is also [SectionList](https://facebook.github.io/react-native/docs/sectionlist.html)

#### The Image Component
[Image](https://facebook.github.io/react-native/docs/image.html) allows for the display of images.
Properties include maximum height/width and ```resizeMode```
If the image is a URI instead of an imported file, it may be necessary to manually set height and width.

#### The Modal Component
The [Modal](https://facebook.github.io/react-native/docs/modal.html) component is a simple way to present content above an enclosing view.

### Babel JavaScript Transformers

[Syntax transformers](https://facebook.github.io/react-native/docs/javascript-environment.html) make writing code more enjoyable by allowing you to use new JavaScript syntax without having to wait for support on all interpreters.

React Native ships with the [Babel JavaScript compiler](https://bablejs.io).

A full list of React Native's enabled transformations can be found in metro-react-native-babel-preset.

ES5

    Reserved Words: promise.catch(function() { });

ES6

    Arrow functions: <C onPress={() => this.setState({pressed: true})} />
    Block scoping: let greeting = 'hi';
    Call spread: Math.max(...array);
    Classes: class C extends React.Component { render() { return <View />; } }
    Constants: const answer = 42;
    Destructuring: var {isActive, style} = this.props;
    for...of: for (var num of [1, 2, 3]) {};
    Modules: import React, { Component } from 'react';
    Computed Properties: var key = 'abc'; var obj = {[key]: 10};
    Object Concise Method: var obj = { method() { return 10; } };
    Object Short Notation: var name = 'vjeux'; var obj = { name };
    Rest Params: function(type, ...args) {};
    Template Literals: var who = 'world'; var str = `Hello ${who}`;

ES8

    Function Trailing Comma: function f(a, b, c,) {};
    Async Functions: async function doStuffAsync() { const foo = await doOtherStuffAsync(); };

Stage 3

    Object Spread: var extended = { ...obj, a: 10 };

Stage 2

    Optional Chaining: var name = obj.user?.name;

Specific

    JSX: <View style={{color: 'red'}} />
    Flow: function foo(x: ?number): string {};

### Polyfills

Many standards functions are also available on all the supported JavaScript runtimes.

Browser

    console.{log, warn, error, info, trace, table, group, groupEnd}
    CommonJS require
    XMLHttpRequest, fetch
    {set, clear}{Timeout, Interval, Immediate}, {request, cancel}AnimationFrame

ES6

    Object.assign
    String.prototype.{startsWith, endsWith, repeat, includes}
    Array.from
    Array.prototype.{find, findIndex}

ES7

    Array.prototype.{includes}

ES8

    Object.{entries, values}

Specific

    __DEV__
