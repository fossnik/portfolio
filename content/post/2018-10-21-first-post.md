---
title: Hello Hugo!
date: 2018-10-21
---

This website is built by a continuous integration (CI/CD) process in under 1 minute by [Hugo](https://gohugo.io), and hosted effortlessly on [GitLab Pages](https://about.gitlab.com/features/pages/)
