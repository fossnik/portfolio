---
title: Brooklyn Urban Tech Week
subtitle: DAOstack Competition in Bushwick
date: 2018-11-10
---

I had a blast at the Brooklyn Hackathon; the experience of coming up to speed on [Solidity](https://solidity.readthedocs.io) and [DAOstack](https://daostack.io) in about a 14 hour period was exhilarating to say the least!
Distributed ledger and smart-contract technology will certainly have profound implications for future of business and democratic systems!

{{< figure src="/BrooklynHackathon1.jpg" >}}
My team won $300 in Ethereum !

{{< figure src="/BrooklynHackathon2.jpg" >}}
Here I am learning about an exciting new technology from a member of the DAOstack team

<!--more-->

{{< figure src="/BrooklynHackathon3.jpg" >}}
Special thanks to the [Bushwick Generator](https://www.thebushwickgenerator.com) for hosting CareerDevs at this event

{{< figure src="/BrooklynHackathon4.jpg" >}}
DAOstack team looks on as Arnell Milhouse annouces contest winners
