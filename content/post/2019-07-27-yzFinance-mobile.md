---
title: "yZ Finance Mobile"
date: 2019-07-27
subtitle: "A React Native Application"
tags: ["React Native", "Redux", "TypeScript", "Express.js", "SQLite"]
---

React-Native mobile app for my [Yahoo-finance web scraper]( {{< relref "/post/2018-11-05-financial-web-scraper.md" >}} ).

{{< figure src="/2019.07.27-yzFinance-1.gif" >}}

# Scan to open
With an Android phone, you can scan this QR code with your [Expo mobile App](https://play.google.com/store/apps/details?id=host.exp.exponent) to load this project immediately.
{{< figure src="/2019.07.27-yzFinance-expo-qr-code.png" >}}
---
Visit this app at it's home on expo.io ([https://expo.io/@fossnik/yz-finance-mobile](https://expo.io/@fossnik/yz-finance-mobile))

---
# Features
- [Redux state management]( {{< relref "/post/2019-02-17-twid_008.md" >}} )
- [Cypress Integration Testing]( {{< relref "/post/2019-08-31-react-integration-testing-cypress.md" >}} )
- [TypeScript]( {{< relref "/post/2019-08-14-react-typescript.md" >}} )
- Queries my [financial web scraper]( {{< relref "/post/2018-11-05-financial-web-scraper.md" >}} ) backend

<!--more-->

Check out the source code at [https://gitlab.com/fossnik/yzfinance-mobile-expo](https://gitlab.com/fossnik/yzfinance-mobile-expo)
# Cypress Tests
 [Cypress Integration Testing]( {{< relref "/post/2019-08-31-react-integration-testing-cypress.md" >}} )
