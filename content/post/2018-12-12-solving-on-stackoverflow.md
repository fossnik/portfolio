---
title: Solving on Stack Overflow
subtitle: How posing technical difficulties to online forums nets me new coding superpowers!
date: 2018-12-12
tags: ["tech tips"]
---
[Stackoverflow.com](https://www.stackoverflow.com) is a website that I had used probably hundreds or thousands of times before I ever considered joining it. I'm already learning a lot; great community to be a part of and contribute to. 
{{< figure src="/stackoverflow-official.svg" >}}

I found help on StackOverflow.com for a problem I was having with ganache-cls. [stackoverflow.com/questions/53751386/](https://stackoverflow.com/questions/53751386/npm-global-install-of-ganache-cli-fails-because-of-sudo-permissions/53771367#53771367)
Learned how to avoid using "sudo" for globally installing npm packages.    
