---
title: "Using AWS DataStore"
date: 2019-12-20
subtitle: "A local data storage solution for AppSync"
tags: ["React.js", "AWS", "DynamoDB", "Amplify"]
draft: true
---

https://docs.aws.amazon.com/cli/latest/userguide/aws-cli.pdf

<!--more-->
