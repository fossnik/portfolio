---
title: Integration testing with Cypress
subtitle: React.js E2E Testing Frameworks
date: 2019-08-31
tags: ["React", "Integration Testing", "Cypress", "E2E Testing", "Appium.io"]
---

{{< figure src="/2019.09.29-yzfinance-cypress-tests.gif" >}} 

## Cypress E2E Integration Testing
The [cypress testing framework](https://www.cypress.io/) is an end-to-end integration test framework for React.js, which is quickly surpassing alternatives such as Jest, Mocha, and Enzyme, Chai, and others.

Unit testing had long been an area of React.js where a stark lack of consensus existed. Myriad different frameworks had been available, all with their own selling points.
Many developers won over by Cypress are now abandoning this patchwork of other *unit testing* tools in droves.


<!--more-->

# Cypress.io

{{< figure src="/logos/2019.08.31-cypress-e2e-testing.jpg" >}} 






# Automation Testing with Appium

{{< figure src="/logos/2019.09.08-appium.png" >}}

## Appium.io Automation
Testing is a repetitive task, and mobile applications require automated testing just as much as their web-based counterparts.
This is where Appium comes in.
Appium is a testing framework that is used for automated testing with Native, mobile web, and hybrid applications.
The Appium API is a cross-platform tool that makes it possible to write tests for both iOS and Android.
Appium can be written in any language (Java, Ruby, Python, PHP, C#, etc) and used for automation purposes without modifying anything.
Testing can be performed on real devices, or simulators / emulators.

<!--more-->
## Selenium
Because Appium is based on the [WebDriver](https://www.seleniumhq.org/projects/webdriver/) framework, it has much in common with the [Selenium web testing framework](https://www.seleniumhq.org/).
This includes the [JSON Wire Protocol](https://subscription.packtpub.com/book/application_development/9781784392482/1/ch01lvl1sec09/the-selenium-json-wire-protocol)

Appium is designed with a client/server architecture in mind. When the Appium server is started, it exposes a [Rest API](https://restfulapi.net/) to the client.

## Elements
Selenium Jars
Java-Client
Appium Server
