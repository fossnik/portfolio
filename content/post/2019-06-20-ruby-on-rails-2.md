---
title: Elegant Ruby
date: 2019-06-20
tags: ["Ruby on Rails"]
---

Ruby lends itself to some syntactically elegant solutions, like in this program that swaps diagonally opposed elements in a matrix.

    def swapDiagonals(matrix)
      matrix.each_with_index.map do | row, row_index |
        row.each_with_index.map do | element, col_index |
          if col_index == row_index
            row[row.length - row_index - 1]
          elsif  col_index == row.length - row_index - 1
            row[row_index]
          else
            element
          end
        end
      end
    end
    
    input:
     [[1,2,3], 
     [4,5,6], 
     [7,8,9]]
    
    output:
    [[3,2,1], 
     [4,5,6], 
     [9,8,7]]

<!--more-->

[My solution](https://app.codesignal.com/challenge/rApnerc87hMTtXB3a?solutionId=NTzHvCmXcHgTLbNaK)
