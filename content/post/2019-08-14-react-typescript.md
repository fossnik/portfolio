---
title: Using TypeScript with React
subtitle: How Strong Typing Helps You Spot Errors You Never Knew Were There 
tags: ["React", "TypeScript"]
date: 2019-08-14
---
{{< figure src="/logos/typescript-graphic.jpg" >}}

## So what is Typing anyway?
To understand what TypeScript is, first you must be familiar with the concept of typing.
JavaScript developers will probably be aware of the difference between Equality (==) and Identity (===) Operators.
These operators differentiate between entities like the string ```"123"``` and the integer ```123```.
This is because numbers and characters are fundamentally stored differently in memory.
In JavaScript, all variables are declared using the keyword ```var``` (or ```const```/```let```).
However, this is not the case in ***strongly typed*** languages, such as ```Java``` and ```C```, which CANNOT declare any variable without an explicitly defined ```data type```.

#### JavaScript is 'interpreted', not 'compiled'
A ```data type``` is a formalization of the way in which information is represented in a computer system, such as the case with Integers and Strings.
Languages such as ```Java``` and ```C``` are referred to as ***strongly typed*** languages, because they depend on these types being defined at compile time, before the high-level language is converted into machine code or byte-code.
JavaScript is an ***interpreted language*** (it is not compiled)
It uses ***dynamic typing*** because, as the name implies, Java***Script*** was created with script-building in mind.
Scripts generally automate the execution of code that is exterior to the script itself (such as compiled programs).
As JavaScript development has become increasingly complex, there has arisen greater need for more explicit typing.
This need was partially met by tools such as [PropTypes](https://www.npmjs.com/package/prop-types).
However, TypeScript, which was created by Microsoft, is especially helpful for validating the consistency of user-designed ***data objects***.

## Type Checking
```Type checking``` is the idea that the shape of a data object remains consistent as it is passed between different parts of a program.
[TypeScript](https://www.typescriptlang.org) extends the JavaScript language, making it possible to explicitly define the shape of objects when they are declared, or passed into / returned from a function.

<!--more-->
When I first started to use TypeScript linting, I immediately found all kinds of subtle errors in my code that I had never realized were there.
We may set out to write a program with a particular idea in mind, but as that idea starts to take shape, we have to change our code to suit disparate purposes, and add new features.
It is not always obvious how changing the shape of an object in one area will affect the code we have used in a myriad of other places.
Even with relatively simple programs, it's not hard to lose track of what is really going on, particularly when working with platforms like [Redux.js](https://redux.js.org), where it is necessary to keep track of references to data objects that are splashed across a constellation of files, all of which need to be modified in concert.
This is where [TypeScript](https://www.typescriptlang.org) really shines.
It can tell us that the data we are passing in is not what is being expected.
This makes it a lot easier to track down minute flaws in our logic that can be difficult to otherwise identify.

## Awesome! So how do I use it?
The [official documentation](https://www.typescriptlang.org/docs/home.html) is a great place to start.

First install the package

    npm install -g typescript
    


Define a variable with an explicit type

    var a: number = myFunctionReturnsInt()
    var b: string = myFunctionReturnsString()

Define a function that takes explicit type and returns explicit type (ES6)

    dayOfWeek = (day: number): string => {
        switch(day) {
            case 1: return "Sunday";
            case 2: return "Monday";
        ...

Define your own data type

    interface Person {
        firstName: string;
        lastName: string;
    }
    
    function greeter(person: Person) {
        return "Hello, " + person.firstName + " " + person.lastName;
    }
    
    let user = { firstName: "Jane", lastName: "User" };
    
    console.log(greeter(user))


## Finding errors you never knew were there

It's quite easy to do something like this:


    <th className='tableHead' colSpan='2'>


You may not ever realize that this is wrong, but fortunately TypeScript is there to call attention to your error, because TypeScript understands that a *number* is expected, not a *string*

{{< figure src="/printscrn-2019.08.26_12:42:53-typescript.png" >}}


Now you can correct it

    <th className='tableHead' colSpan={2}>
