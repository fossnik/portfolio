---
title: As We May Think
subtitle: Ideas of Vannevar Bush in practice
date: 2019-03-31
tags: ["Vannevar Bush", "Working Smarter", "Unit Testing"]
---

## Referencing your entire knowledge-base
[Vannevar Bush](https://en.wikipedia.org/wiki/Vannevar_Bush) in his seminal essay [As We May Think](https://en.wikipedia.org/wiki/As_We_May_Think)

One of my very favorite strategies is prototyping all of my solutions with unit tests, and having all of my solutions locally,
because with a simple text search I can easily reference code I have used in the past.

### Refer to past work for convenience
Languages like Java can be frustrating
 Even the most quotidian operations are often difficult to remember.
  For example, I KNOW I have converted an int array to an integer list in the past, dozens or hundreds of times.
   Especially with Java, I never expect anything to work exactly as anticipated, so I reference past work, and put it to the test (with [unit testing](https://en.wikipedia.org/wiki/Unit_testing))
  
### How the heck did I do that?
 When your past work is indexed, you can instantly scour reams of code to derive solutions from existing content. That's working smart.
    
<!--more-->
