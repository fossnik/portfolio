---
title: "Revisiting React (the Docs)"
date: 2019-10-23
subtitle: "Brushing up on documentation"
tags: ["React.js", "Documentation"]
draft: true
---

Perhaps more than any other facet of a given technology, good *documentation* is key to a good experience.
This is especially true of rapidly evolving technologies such as React.js, where the span of a mere months can be the difference between code that works, and a woefully broken experience.

This week I decided to revisit [React.js documentation](https://reactjs.org/docs/hello-world.html) to solidify existing knowledge that I may have gleaned from other sources, and to be sure that my existing understanding is as rock-solid as I need it to be.

Keeping up to date is absolutely essential in this arena, so even though I have been telescoped on lock at React Native for a few months now, it will be a great benefit to revisit the React.js framework upon which it is based to see what changes may have been visited upon this platform by those crazy fellas at Facebook. 

In order to truly consider myself a subject matter expert, it is necessary to understand every aspect of the technology, such that nothing is outside the scope of my understanding.
To this end, I endeavor to periodically review documentation and keep myself abreast of upstream deprecations and new features.
additionally, in technologies that are such rapidly moving targets as is the case of React.js under perpectual internal development and use at Facebook.
