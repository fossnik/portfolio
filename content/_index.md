Welcome to my portfolio
---
I have [designed this blog](/post/2018-10-31-why-hugo) to showcase projects and chronicle my growth as a software developer. 

#### My other platforms:

- [Twitter](https://twitter.com/zack_hartmann)
- [GitLab](https://gitlab.com/fossnik)
- [LinkedIn](https://linkedin.com/in/zackhartmann)
